#define _CRT_SECURE_NO_DEPRECATE

#include <iostream>
#include <string>
#include <cctype>



using namespace std;


int main() {
	string chain;

	cout << "enter a text -> ";
	getline(cin, chain);

	int len = chain.length();

	for (int i = 0; i < len; i++)
		if (ispunct(chain[i])) {
			chain.replace(i, 1, " ");
		}

	char* string = new char [len +1];
	
	chain.copy(string, len);
	string[len] = '\0';

	char* tokens = strtok(string, " ");
	
	while (tokens != NULL) {
		cout << tokens << endl;
		tokens = strtok(NULL, " ");
	}

	delete[] string;
	delete[] tokens;
}