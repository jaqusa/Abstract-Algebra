#include <iostream>
#include <string>

using namespace std;


int main() {
	string chain1, chain2;

	do {
		cout << "first string with even length  -> ";
		getline(cin, chain1);
	} while (chain1.length() % 2 != 0);

	cout << "second string -> ";
	getline(cin, chain2);

	cout << chain1.insert(chain1.length() / 2, chain2) << endl;


}