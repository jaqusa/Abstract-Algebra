#include <iostream>
#include <string>

using namespace std;


string findAndReplace(string chain1, string chain2, string chain3) {
	int start = 0;
	int len = chain2.length();
	while ((start = chain1.find(chain2, start)) != -1) {
		chain1.replace(start, len, chain3);
		start += len;
	}

	return chain1;
}

int main() {
	string chain1, chain2, chain3;

	cout << "first string -> " ;
	getline(cin, chain1);
	cout << "second string -> " ;
	getline(cin, chain2);
	cout << "third string -> " ;
	getline(cin, chain3);

	cout << findAndReplace(chain1, chain2, chain3);

}