#include <iostream>
#include <string.h>

using namespace std;

string toUpperOrLower(string option, string chain) {
	string newChain =  chain;

	for (int i = 0; i < chain.length(); i++) {
		newChain[i] = option == "toUpper"? toupper(chain[i]):tolower(chain[i]);
	}

	return newChain;
}

int main() {
	string chainLower = "string lower", chainUpper="STRING UPPER";
	
	cout << toUpperOrLower("toUpper", chainLower) << endl;
	cout << toUpperOrLower("toLower", chainUpper) << endl;


}