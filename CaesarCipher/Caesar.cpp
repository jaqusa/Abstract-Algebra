#include "Caesar.h"
#include <string>

using namespace std;

Caesar::Caesar(): _length(alphabet.length()) {
	_key = 3;
}

string Caesar::Decrypt(string encrypted_message) {
	string  decrypted_message;
	
	for (int i = 0; i < encrypted_message.length(); i++) {
		int n = alphabet.find(encrypted_message[i]) - _key;
		decrypted_message += alphabet[n < 0 ?  n + _length : n];
	}

	return decrypted_message;
}

string Caesar::Encrypt(string message) {
	string  encrypted_message;

	for (int i = 0; i < message.length(); i++) {
		int n = alphabet.find(message[i]) + _key;
		encrypted_message += alphabet[n > _length ? n % _length : n ];
	}

	return encrypted_message;
}
