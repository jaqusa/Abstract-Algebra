#pragma once
#include <iostream>

class Caesar
{	
	int _key;
	const int _length ;
public:
	Caesar();
	std::string Encrypt(std::string message);
	std::string Decrypt(std::string encrypted_message);
	std::string alphabet = "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ,.";
};



