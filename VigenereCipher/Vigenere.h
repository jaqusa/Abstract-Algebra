﻿#pragma once
#include <iostream>

class Vigenere
{
	std::string _key;
public:
	Vigenere(std::string key,int selected_alphabet);
	std::string Encrypt(std::string message);
	std::string Decrypt(std::string encrypted_message);
	std::string alphabet = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
	std::string numeric_alphabet = "abcdefghijklmnopqrstuvwxyz -ABCDEFGHIJKLMNOPQRSTUVWXYZ,.0123456789";
	std::string ascii_alphabet = "␀␁␂␃␄␅␆␇␈␉␊␋␌␍␎␏␐␑␒␓␔␕␖␗␘␙␚␛␜␝␞␟ !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~␡";
};

