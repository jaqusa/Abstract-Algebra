#include <iostream>
#include <string>
#include "Vigenere.h"

using namespace std;

int main() {
	int alphabet, option;
	string message, cipher_text, deciphered_text;

	cout << "Escoja un alfabeto (1:Numerico, 2:ASCII): " ;
	cin>> alphabet ;

	Vigenere encoder("Javier Arturo Quinte Sanchez 161-10-37659", alphabet);
	Vigenere decoder("Javier Arturo Quinte Sanchez 161-10-37659", alphabet);

	cin.ignore(1000, '\n');
	cout << "Enter a message: ";
	getline(cin, message);
	cout << endl;

	cipher_text = encoder.Encrypt(message);
	cout << "Cipher text: " << endl << cipher_text << endl;
	deciphered_text = decoder.Decrypt(cipher_text);
	cout << "Deciphered text: " << endl << deciphered_text << endl;
	


	return 0;
}