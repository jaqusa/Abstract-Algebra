#include "Vigenere.h"
#include <string>

using namespace std;

Vigenere::Vigenere(string key, int selected_alphabet) :_key(key) {
	switch (selected_alphabet)
	{
	case 1:
		alphabet = numeric_alphabet;
		break;
	case 2:
		alphabet = ascii_alphabet;
		break;
	default:
		break;
	}

}

string Vigenere::Encrypt(string message)
{
	string  encrypted_message;
	int length_alphabet = alphabet.length();

	for (int i = 0; i < message.length(); i++) {
		int length_key = _key.length();
		char key_character = length_key <= i ? _key[i % length_key] : _key[i];
		int n = (alphabet.find(message[i]) + alphabet.find(key_character)) % length_alphabet;
		encrypted_message += alphabet[n];
	}

	return encrypted_message;
}


string Vigenere::Decrypt(string encrypted_message)
{
	string  decrypted_message;
	int length_alphabet = alphabet.length();

	for (int i = 0; i < encrypted_message.length(); i++) {
		int length_key = _key.length();
		char key_character = length_key <= i ? _key[i % length_key] : _key[i];
		int encrypted_character_position = alphabet.find(encrypted_message[i]);
		int key_character_position = alphabet.find(key_character);
		int decrypted_letter_position = (encrypted_character_position - key_character_position) % length_alphabet;
		decrypted_message += alphabet[decrypted_letter_position < 0 ? decrypted_letter_position + length_alphabet : decrypted_letter_position];
	}

	return  decrypted_message;
}
